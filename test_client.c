#include "data.h"


Routing_Table tableInfo[MAX_NODES];

void init()
{
	int i;
	for(i=0;i<MAX_NODES;i++)
	{
		tableInfo[i].mycport=0;
		tableInfo[i].mydport=0;
		tableInfo[i].myid=0;
	}
}

void create()
{
	FILE *fp;
	char *elements = NULL, line[100];
	int i,temp;
	fp = fopen("config_file", "r");
	if (fp == NULL)
	{
		printf("Could not open file %s", "config_file");
		exit(1);
	}
	i=0;
	while (fgets(line, 100, fp) != NULL)
	{
		line[strlen(line) - 1] = '\0';
		elements = strtok(line, " ");
		tableInfo[i].myid = atoi(elements);  
		elements = strtok(NULL, " ");
		strcpy(tableInfo[i].myname, elements);
		elements = strtok(NULL, " ");
		tableInfo[i].mycport=atoi(elements);
		elements = strtok(NULL, " ");
		tableInfo[i].mydport=atoi(elements);
		elements = strtok(NULL, " ");
		while(elements != NULL)
		{
			elements = strtok(NULL, " ");
		}
		i++;

	}
	fclose(fp);
}
void *createData(int input1, int input2)
{
	char nodeName[50];
	int nodePort;
	int i;
	int found = 0;
	char append[5];
	Data_Packet data;
	data.sid = input1;
	data.did = input2;
	data.tty = 15;
	data.type = 1;
	sprintf(append, "%d",input1);
	strcpy(data.hops, append);
	//printf("%s\n", append);
	//printf("MAX_NODES -- %d\n", MAX_NODES);
	for(i=0;i<MAX_NODES;i++)
	{ 
		//printf("Comparing %d with %d\n", tableInfo[i].myid, input1);
		if(tableInfo[i].myid == input1)
		{
			strcpy(nodeName, tableInfo[i].myname);
			nodePort = tableInfo[i].mydport;
			found = 1;
			break;
		}
	}
	if(found == 1)
	{
		strcpy(data.data, "********Hello**********");
		printf("\n********************\n");
		printf("\nData Packet Created!\n");
		printf("\n********************\n");
		sendudpdatamsg(nodeName, &data, nodePort);
	}
	else
		exit(1);
}

void sendudpctrlmsg(char *server,  Distance_Vector *msg, int port)
{
	struct sockaddr_in serveraddr;
	char ip[50];
	int size=sizeof(serveraddr),sockfd,n;
	sockfd=socket(AF_INET,SOCK_DGRAM,0);
	if (sockfd<0)
	{
		printf("Unable to open socket discriptor, msg sending error\n");
		exit(1);
	}
	bzero((char *)&serveraddr,sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_port = htons(port);
	if(nametoip(server,ip)!=0)
		printf("name to ip failed \n");
	serveraddr.sin_addr.s_addr = inet_addr(ip);
	sendto(sockfd,msg,sizeof(Distance_Vector),0,(struct sockaddr *)&serveraddr,size);
	close(sockfd);
}


void sendudpdatamsg(char *server,  Data_Packet *msg, int port)
{
	struct sockaddr_in serveraddr;
	char ip[50];
	int size=sizeof(serveraddr),sockfd,n;
	sockfd=socket(AF_INET,SOCK_DGRAM,0);
	if (sockfd<0)
	{
		printf("Unable to open socket discriptor, msg sending error\n");
		exit(1);
	}
	bzero((char *)&serveraddr,sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_port = htons(port);
	if(nametoip(server,ip)!=0)
		printf("name to ip failed \n");
	serveraddr.sin_addr.s_addr = inet_addr(ip);
	sendto(sockfd,msg,sizeof(Data_Packet),0,(struct sockaddr *)&serveraddr,size);					close(sockfd);
}

int nametoip(char *name, char *ip)
{
	struct hostent *host;
	int i;
	struct in_addr **ipaddress;
	if ( (host=gethostbyname(name)) == NULL)
	{
		printf("gethostbyname error");
		return 1;
	}
	ipaddress = (struct in_addr**)host->h_addr_list;
	for(i=0;ipaddress[i]!=NULL;i++)
	{
		strcpy(ip, inet_ntoa(*ipaddress[i]));
		return 0;
	}
}



void updatelink(int choice, int input1, int input2)
{
	char nodeName[50];
	int nodePort,i;
	Distance_Vector msg;
	msg.from=choice;
	msg.dest=input2;
	msg.nexthop=input1;
	if(choice==3)
	{
		for(i=0;i<MAX_NODES;i++)
		{
			if(tableInfo[i].myid != 0)
				sendudpctrlmsg(tableInfo[i].myname,&msg,tableInfo[i].mycport);
		}
	}
	else
	{
		for(i=0;i<MAX_NODES;i++)
		{
			if(tableInfo[i].myid == input1)
			{
				strcpy(nodeName, tableInfo[i].myname);
				nodePort = tableInfo[i].mycport;
				break;
			}

			//exit(1);
		}
		sendudpctrlmsg(nodeName,&msg,nodePort);
	}
}

int main(int argc, char *argv[])
{
	int input1, input2;
	int i;
	char inputString[50],line[60];
	int choice;
	init();
	create();

	while(1)
	{
		printf("\nPlease enter your choice of operation\n");
		printf("\n1: Create-Link\t 2: Remove-Link\t 3: Generate-Packet\t 4: Print-Distance-Vector-Table\t 5: Exit\n");
		scanf("%d", &choice);
		if(choice != 4 && choice !=5)
		{
			printf("Please enter the Source and Destination nodes\n");
			scanf("%d%d",&input1, &input2);
		}
		switch(choice)
		{
			case 3 : createData(input1, input2);
				 break;
			case 2 :
			case 1 :
				 printf("\n******************************************\n");
				 printf("\nControl Packet Created and sent to %d & %d\n",input1,input2);
				 printf("\n******************************************\n");
				 updatelink(choice,input1,input2);
				 updatelink(choice,input2,input1);
				 break;
			case 4:  updatelink(3,0,0);
				 break;
			case 5 : exit(0);
				 break;
			default: printf("\nwrong choice\n");

		}
	}
}

