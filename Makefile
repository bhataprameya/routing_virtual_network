all: create_process.c test_client.c
	gcc -g -pthread create_process.c -o start_node
	gcc -g test_client.c -o test_client
clean:
	rm -f *.o *.out start_node test_client
