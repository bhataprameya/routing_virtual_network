#include "data.h"
pthread_mutex_t mutex;
void create()
{
	FILE *fp;
	char *elements = NULL, line[100];
	int i,temp;
	init();
	fp = fopen("config_file", "r");
	if (fp == NULL)
	{
		printf("Could not open file %s", "config_file");
		exit(1);
	}
	while (fgets(line, 100, fp) != NULL)
	{
		line[strlen(line) - 1] = '\0';
		elements = strtok(line, " ");
		if(atoi(elements)==table.myid)
		{       i=0;
			elements = strtok(NULL, " ");
			strcpy(table.myname, elements);
			elements = strtok(NULL, " ");
			table.mycport=atoi(elements);
			elements = strtok(NULL, " ");
			table.mydport=atoi(elements);
			elements = strtok(NULL, " ");
			while(elements != NULL)
			{
				table.mylinks[atoi(elements)]=1;
				elements = strtok(NULL, " ");
				i++;
			}
			break;
		}
		else
			continue;

	}
	fseek(fp,0,SEEK_SET);           //Sets file pointer to starting of file
	while (fgets(line, 100, fp) != NULL)
	{
		line[strlen(line) - 1] = '\0';
		elements = strtok(line, " ");
		if(table.mylinks[atoi(elements)]==1)
		{       temp=atoi(elements);
			elements = strtok(NULL, " ");
			strcpy(table.namelist[temp], elements);
			elements = strtok(NULL, " ");
			table.cportlist[temp]=atoi(elements);
			elements = strtok(NULL, " ");
			table.dportlist[temp]=atoi(elements);
		}

	}
	fclose(fp);
	createdtable();
}

void createdtable()
{
	int i;
	for(i=0;i< MAX_NODES;i++)
	{
		if(table.mylinks[i]==1)
		{
			table.dest[i]=1;
			table.nexthop[i]=i;
			table.distance[i]=1;
		}
	}
	table.dest[table.myid]=-1;
	table.nexthop[table.myid]=-1;
	table.distance[table.myid]=-1;
}


void init()
{
	int i;
	for(i=0;i<MAX_NODES;i++)
	{
		table.mylinks[i]=0;
		table.cportlist[i]=0;
		table.dportlist[i]=0;	
		table.dest[i]=0;
		table.nexthop[i]=0;
		table.distance[i]=0;
	}
}


void print()
{
	int i;
	printf("My details are: %d %s %d %d",table.myid,table.myname,table.mycport,table.mydport);
	/*	for(i=0;i<MAX_NODES;i++)
		{
		if(table.mylinks[i]==1)
		printf(" %d", i);
		}
		printf("\n");
		for(i=0;i<MAX_NODES;i++)
		{
		if(table.mylinks[i]==1)
		printf(" %d %s %d %d\n",i,table.namelist[i],table.cportlist[i],table.dportlist[i]);
		}*/
	printf("\n\n--------------Distance Vector Table of Node %d-------------\n",table.myid);
	for(i=0;i<MAX_NODES;i++)
	{
		if(table.dest[i]==1||table.dest[i]==-1)
			printf("(%d, %d, %d)\n", i,table.nexthop[i],table.distance[i]);
	}
	printf("\n\n------------------------End Of Table------------------------\n");


}

void* waitservercport()
{
	int sockfd,size,n,portno=table.mycport;
	fd_set fd;
	pthread_t tid1, tid2, tid3;
	struct timeval time;
	Distance_Vector v;
	struct sockaddr_in myaddr, senderaddr;

	sockfd=socket(AF_INET,SOCK_DGRAM,0);
	if (sockfd<0)
	{
		printf("Unable to open socket discriptor\n");
		exit(1);
	}
	bzero((char *)&myaddr,sizeof(myaddr));
	myaddr.sin_family=AF_INET;
	myaddr.sin_addr.s_addr=htonl(INADDR_ANY);
	myaddr.sin_port=htons((unsigned short)portno);

	if (bind(sockfd,(struct sockaddr *)&myaddr,sizeof(myaddr))<0)
	{
		//printf("control");
		printf("binding failed\n");
		exit(1);
	}
	size = sizeof(myaddr);
	FD_ZERO(&fd);
	printf("\n****************************\n");
	printf("Node %d Created Successfully",table.myid);
	printf("\n****************************\n");
	while(1)
	{
		time.tv_sec = DISTANCE_VECTOR_EXCHANGE_TIME;
		FD_SET(sockfd, &fd);
		bzero(&v,sizeof(v));
		if(select(sockfd+1,&fd,NULL,NULL,&time)<0)
			continue;
		//printf("select done\n");
		if(FD_ISSET(sockfd, &fd))
		{
			//printf("inside if\n");
			n=recvfrom(sockfd,&v,sizeof(v),0,(struct sockaddr *)&senderaddr,&size);
			if (n<0)
				printf("recvfrom error\n");

			//printf("Msg received from  %d\n",v.sendernode);
			//pthread_create(&tid3, NULL, updatetable, (void *)&v);
			//pthread_detach(tid3);
			//pthread_create(&tid2, NULL, controlfunction,NULL);
			updatetable(&v);
			//print();



		}
		else 
		{	
			//printf("inside else\n");
			//pthread_create(&tid1, NULL, controlfunction,NULL);
			//pthread_detach(tid1);
			controlfunction();
		}
		FD_CLR(sockfd, &fd);
	}
	close(sockfd);


}

void* waitserverdport()
{
	int sockfd,size,n,portno=table.mydport;
	char append[10];
	Data_Packet data;
	fd_set fd;
	struct sockaddr_in myaddr, senderaddr;

	//printf("%d\n", portno);
	//printf("entered data port function\n");
	sockfd=socket(AF_INET,SOCK_DGRAM,0);
	if (sockfd<0)
	{
		printf("Unable to open socket discriptor\n");
		exit(1);
	}
	bzero((char *)&myaddr,sizeof(myaddr));
	myaddr.sin_family=AF_INET;
	myaddr.sin_addr.s_addr=htonl(INADDR_ANY);
	myaddr.sin_port=htons((unsigned short)portno);

	if (bind(sockfd,(struct sockaddr *)&myaddr,sizeof(myaddr))<0)
	{
		//printf("data");
		printf("binding failed\n");
		exit(1);
	}
	size = sizeof(myaddr);
	if(data.did == table.myid);
	FD_ZERO(&fd);
	while(1)
	{
		//printf("Inside while\n");
		FD_SET(sockfd, &fd);
		bzero(&data,sizeof(data));
		if(select(sockfd+1,&fd,NULL,NULL,NULL)<0)
			continue;
		//printf("select failed\n");
		//printf("select done\n");
		if(FD_ISSET(sockfd,&fd))
		{
			n=recvfrom(sockfd,&data,sizeof(data),0,(struct sockaddr *)&senderaddr,&size); //receiving data packet
			if (n<0)
				printf("recvfrom error\n");
			if(data.type==0)
				data.tty--;
			//printf("\n******************\n");
			//printf("\nPacket received\n");
			//printf("\n******************\n");
			if(data.did == table.myid && data.type == 0)
			{
				printf("\n********************************************************************\n");
				printf("\nData Packet Received at Destination node %d in port %d!!\n", table.myid, table.mydport);
				printf("\n********************************************************************\n");
				printf("\nData Route --> %s\n",data.hops);
				printf("\n********************************************************************\n");
				printf("\nData Details at Destination is as follows:\n");
				printf("Data Packet ID = %d\n", data.pid);
				printf("Data SID = %d\n", data.sid);
				printf("Data DID = %d\n", data.did);
				printf("Data message = %s\n", data.data);
				printf("Destination tty = %d", data.tty);
				printf("\n********************************************************************\n");
			}
			else
			{
				//printf("Data Packet Received of different destination\n");
				if(data.type == 1)
				{
					counter++;
					data.type = 0;
					data.pid = counter;
					strcpy(data.data, "Hi Networks");
					printf("\n************************************************************\n");
					printf("\nData Details at source are as follows:\n");
					printf("Data Packet ID = %d\n", data.pid);
					printf("Data SID = %d\n", data.sid);
					printf("Data DID = %d\n", data.did);
					printf("Data message = %s\n", data.data);
					printf("Source tty = %d\n", data.tty);
					printf("\n************************************************************\n");
				}
				if(data.tty <= 0)
				{	
					printf("\n************************************************\n");				
					printf("\nData Packet Dropped due insufficient details\n");
					printf("\n************************************************\n");
					continue;	
				}
				if(table.dest[data.did]==0)		//Check if Have a node entry in vector table
				{
					printf("\n**************************************************************************************\n");
					printf("\npacket being forwarded to same port as no entry in the vector node for the destination\n");
					printf("\n**************************************************************************************\n");
					sendudpdatamsg(table.myname,&data,table.mydport);
				}
				else
				{


					pthread_mutex_lock(&mutex);
					sprintf(append, " -> %d", table.nexthop[data.did]);
					strcat(data.hops, append);

					printf("\n************************************************************************************************************************************************************\n");
					printf("\nPacket being forwarded from node %d port %d to node %d port %d\n", table.myid, table.mydport, table.nexthop[data.did], table.dportlist[table.nexthop[data.did]]);
					printf("\n************************************************************************************************************************************************************\n");
					sendudpdatamsg(table.namelist[table.nexthop[data.did]], &data, table.dportlist[table.nexthop[data.did]]);
					pthread_mutex_unlock(&mutex);
				}			
			}
		}
		FD_CLR(sockfd, &fd);
	}
	close(sockfd);
}

void sendudpdatamsg(char *server,  Data_Packet *msg, int port)
{
	struct sockaddr_in serveraddr;
	char ip[50];
	int size=sizeof(serveraddr),sockfd,n;
	sockfd=socket(AF_INET,SOCK_DGRAM,0);
	if (sockfd<0)
	{
		printf("Unable to open socket discriptor, msg sending error\n");
		exit(1);
	}
	bzero((char *)&serveraddr,sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_port = htons(port);
	if(nametoip(server,ip)!=0)
		printf("name to ip failed \n");
	serveraddr.sin_addr.s_addr = inet_addr(ip);
	//printf("Printing values of sid -- %d, did -- %d, tty -- %d, data -- %s\n",msg->sid, msg->did, msg->tty, msg->data);
	sendto(sockfd,msg,sizeof(Data_Packet),0,(struct sockaddr *)&serveraddr,size);
	close(sockfd);
}

void sendudpctrlmsg(char *server,  Distance_Vector *msg, int port)
{
	struct sockaddr_in serveraddr;
	char ip[50];
	int size=sizeof(serveraddr),sockfd,n;
	sockfd=socket(AF_INET,SOCK_DGRAM,0);
	if (sockfd<0)
	{
		printf("Unable to open socket discriptor, msg sending error\n");
		exit(1);
	}
	bzero((char *)&serveraddr,sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_port = htons(port);
	if(nametoip(server,ip)!=0)
		printf("name to ip failed \n");
	serveraddr.sin_addr.s_addr = inet_addr(ip);
	sendto(sockfd,msg,sizeof(Distance_Vector),0,(struct sockaddr *)&serveraddr,size);
	close(sockfd);
}

int nametoip(char *name, char *ip)
{
	struct hostent *host;
	int i;
	struct in_addr **ipaddress;
	if ( (host=gethostbyname(name)) == NULL)
	{
		printf("gethostbyname error");
		return 1;
	}
	ipaddress = (struct in_addr**)host->h_addr_list;
	for(i=0;ipaddress[i]!=NULL;i++)
	{
		strcpy(ip, inet_ntoa(*ipaddress[i]));
		return 0;
	}
}

void *updatetable(void *updated_data)

{	
	Distance_Vector *data_update=(Distance_Vector*)updated_data;
	Distance_Vector data=*data_update;
	int receivednode=data.sendernode, i;
	pthread_mutex_lock(&mutex);
	if(data.from==1||data.from==2||data.from==3)
	{
		if(data.from==1 && table.mylinks[data.dest]==0)
		{
			table.dest[data.dest]=1;
			table.nexthop[data.dest]=data.dest;
			table.distance[data.dest]=1;
			table.mylinks[data.dest]=1;
			//pthread_mutex_unlock(&mutex);		//Unlock
			readdatafromfile(data.dest);		//gets the name cport and dport from the config file
			singlemsgcontrolfunction(data.dest);
		}
		else if(data.from==2 && table.mylinks[data.dest]==1)
		{
			table.dest[data.dest]=0;
			table.nexthop[data.dest]=0;
			table.distance[data.dest]=0;
			table.mylinks[data.dest]=0;
			singlemsgcontrolfunction(data.dest);
			for(i=0;i<MAX_NODES; i++)
			{
				if(table.dest[i]==1 && table.nexthop[i]==data.dest)
				{
					table.distance[i]=0;
					table.nexthop[i]=0;
					table.dest[i]=0;
					singlemsgcontrolfunction(i);

				}
			}
		}
		else if(data.from==3)
		{
			print();
		}
	}
	else if(data.dest!=table.myid)
	{
		if(data.nexthop==-1 && (table.mylinks[data.dest]!=1 || table.distance[data.dest]!=1) )	//nebour node
		{	
			table.dest[data.dest]=1;
			table.nexthop[data.dest]=receivednode;
			table.distance[data.dest]=1;
			table.mylinks[data.dest]=1;
			table.cportlist[data.dest]=data.cport;
			table.dportlist[data.dest]=data.dport;
			//strcpy(table.namelist[data.dest], data.name);
			singlemsgcontrolfunction(data.dest);
		}
		else if(table.dest[data.dest]==0 && data.distance!=0 && data.nexthop!=0 )		//case 1
		{
			table.dest[data.dest]=1;
			table.nexthop[data.dest]=receivednode;
			table.distance[data.dest]=data.distance+1;
			singlemsgcontrolfunction(data.dest);
		}
		else if(table.dest[data.dest]==1 && receivednode==table.nexthop[data.dest])	//case 3
		{
			if(data.distance==0 && data.nexthop==0)
			{
				table.distance[data.dest]=0;
				table.nexthop[data.dest]=0;
				table.dest[data.dest]=0;
				singlemsgcontrolfunction(data.dest);
			}
			else //if(table.distance[data.dest]!=data.distance+1)
			{
				//printf("11111111111\n");
				table.distance[data.dest]=data.distance+1;
				singlemsgcontrolfunction(data.dest);
			}
		}
		else										//Case 2
		{
			if(data.distance+1<table.distance[data.dest] && data.distance!=0 )
			{
				//printf("2222222222\n");
				table.nexthop[data.dest]=receivednode;
				table.distance[data.dest]=data.distance+1;
				singlemsgcontrolfunction(data.dest);
			}
			//singlemsgcontrolfunction(data.dest);
		}
	}
	pthread_mutex_unlock(&mutex);
	//print();
}

void readdatafromfile(int id)
{
	FILE *fp;
	char *elements = NULL, line[100];
	int i,temp;
	fp = fopen("config_file", "r");
	if (fp == NULL)
	{
		printf("Could not open file %s", "config_file");
		exit(1);
	}
	while (fgets(line, 100, fp) != NULL)
	{
		line[strlen(line) - 1] = '\0';
		elements = strtok(line, " ");
		if(atoi(elements)==id)
		{   	
			elements = strtok(NULL, " ");
			strcpy(table.namelist[id], elements);
			elements = strtok(NULL, " ");
			table.cportlist[id]=atoi(elements);
			elements = strtok(NULL, " ");
			table.dportlist[id]=atoi(elements);
		} 
	}
	fclose(fp);
}

void *controlfunction()
{
	int i,j,count=0;
	Distance_Vector v;
	for(i=0;i<MAX_NODES; i++)
	{	
		if(table.mylinks[i]==0)			//Check if its nebouring node
			continue;	
		for(j=0;j<MAX_NODES;j++)
		{
			if(table.dest[j]==0)		//Check if Have a node entry in vector table
				continue;
			if(table.dest[j]==-1)
			{
				v.cport=table.mycport;
				v.dport=table.mydport;
				strcpy(v.name,table.myname);
			}
			else
			{
				v.cport=0;
				v.dport=0;
				//	bzero(v.name,50);
			}
			v.dest=j;
			v.nexthop=table.nexthop[j];
			if(table.distance[j]==-1)
			{
				v.distance=0;
			}
			else
			{
				v.distance=table.distance[j];
			}
			v.sendernode=table.myid;
			v.from=0;
			sendudpctrlmsg(table.namelist[i],&v,table.cportlist[i]);
			//printf("sending msg count=%d\n",count++);
		}
	}
}

void *singlemsgcontrolfunction(int j)
{
	int i,count=0;
	Distance_Vector v;
	for(i=0;i<MAX_NODES; i++)
	{
		if(table.mylinks[i]==0)                 //Check if its nebouring node
			continue;
		if(table.dest[j]==-1)
		{
			v.cport=table.mycport;
			v.dport=table.mydport;
			strcpy(v.name,table.myname);
		}
		else
		{
			v.cport=0;
			v.dport=0;
			//      bzero(v.name,50);
		}
		v.dest=j;
		v.nexthop=table.nexthop[j];
		if(table.distance[j]==-1)
		{
			v.distance=0;
		}
		else
		{
			v.distance=table.distance[j];
		}
		v.sendernode=table.myid;
		v.from=0;
		sendudpctrlmsg(table.namelist[i],&v,table.cportlist[i]);
		//printf("sending msg count=%d\n",count++);
	}
}


void *createData()
{
	Data_Packet data;

	data.sid = table.myid;
	data.did = 2;
	data.tty = 15;
	strcpy(data.data, "Sending hello\0");
	if(table.myid != data.did)
	{
		printf("Data packet created, forwarding process initiated\n ttl value is %d and data is %s", data.tty, data.data);
		sendudpdatamsg(table.myname, &data, table.mydport);
	}
}
