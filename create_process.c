#include "data.h"
#include "utility.c"


int main(int argc, char *argv[])
{	
	pthread_t tid1, tid2;
	if (argc != 2)
	{
		printf("Please enter the node number as a command line parameter\n");
		exit(1);
	}
	init();
	table.myid=atoi(argv[1]);
	create();
	//print();
	pthread_create(&tid1,NULL,waitservercport,NULL);	//Start controlport thread
	//	waitservercport();
	pthread_create(&tid2,NULL,waitserverdport,NULL);	//Start data port thread
	//createData();
	//waitserver(3333);
	//	sendudpdmsg("remote02.cs.binghamton.edu", "hi this is remote02", 3333);

	pthread_join(tid1,NULL);
	pthread_join(tid2,NULL);
	//	pthread_kill(tid1,SIGINT);
	//	pthread_kill(tid2,SIGINT);
}
