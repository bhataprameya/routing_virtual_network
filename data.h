#ifndef DATA
#define DATA

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <signal.h>
#define MAX_NODES 10 //Maximum no of nodes possible in topology
#define DISTANCE_VECTOR_EXCHANGE_TIME 25 	//Time frequency for which distance vector is exchanged
//distance vector data to be passed from one node to another

int counter = 0;
typedef struct
{
	int dest;
	int nexthop;
	int distance;
	int cport;
	int dport;
	char name[50];
	int sendernode;
	int from;	//if 0 sent from other node, If 1 sent from test_client with create-link msg and if 2 sent from test_client with remove-link msg
}Distance_Vector;

typedef struct
{
	int sid;
	int did;
	int pid;
	int tty;
	char data[1024];
	int type;
	char hops[50];
}Data_Packet;



typedef struct
{       //My node property
	int myid;
	int mycport;
	int mydport;
	int mylinks[MAX_NODES];
	char myname[50];
	//propertys of the nodes directly linked to me
	int cportlist[MAX_NODES];
	int dportlist[MAX_NODES];
	char namelist[MAX_NODES][50];

	//Distance vector entrys

	int dest[MAX_NODES];
	int nexthop[MAX_NODES];
	int distance[MAX_NODES];

}Routing_Table;
Routing_Table table;





void *controlfunction();
void *singlemsgcontrolfunction();
void datafunction();
void init();                                //Function to initialize all the routing table variables
void create(void);                              //Finction to fill data to routing table
void print(void);                               //Function to print the routing table
void* waitservercport();                         //function to wait & send for UDP msg CPORT
void* waitserverdport();                         //function to wait & send for UDP msg DPORT
void sendupctrlmsg();			         //Function to send Udp messeage
void sendudpdatamsg();
int nametoip(char*,char*);                      //converts server name to ip address
void *updatetable();		//Update distance vector table of my node based on the content
void createdtable();				//Create distance vector table
void *createData();
void updatelink();
void readdatafromfile();			//update data for neawly created function from config file
#endif
